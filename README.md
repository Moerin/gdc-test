# GDCTEST 

GDCTEST is a flask application bears by nginx + uswgi + flask stack and brought to by the [Tiangolo](https://github.com/tiangolo/uwsgi-nginx-flask-docker) docker image.

# How to use!
- install docker engine and docker-compose

- create .env file to specify required ENV variables in the current directory (same level of docker-compose.yml)

| ENV NAME | VALUE |
| ------ | ------ |
| ETHERNET_INTERFACE | eg. 127.0.0.1 |
| PORT | eg. 80 |
| AWS_SECRET_ACCESS_KEY | your secret key used by aws cli |
| AWS_ACCESS_KEY_ID | your key id used by aws cli |
| AWS_DEFAULT_REGION | your choosen region |
| WORKER | your desired number of process for nginx |

- docker-compose launch
```sh
$ cd gdc-test
$ docker-compose -d
```

- Verify the deployment by navigating to your server address in your preferred browser.
```sh
127.0.0.1
```

- Endpoint '/' for list of deployments and '/DEPLOYEMENT_ID' for detailed data on EC2 deployment.
```sh
127.0.0.1 # for list of deployement ID
```

```sh
127.0.0.1/DEPLOYMENT_ID # detailled version of EC2 deployment
```