FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7

# AWS SDK
RUN pip install boto3==1.5.31
COPY ./app /app
