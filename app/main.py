import boto3
from flask import Flask
from flask import render_template

app = Flask(__name__)


@app.route("/")
def main():
    """ The purpose of this route is to list all deployments id """
    client = boto3.client('codedeploy')
    response = client.list_deployments()

    return render_template('deployments.html', items=response['deployments'])


@app.route("/<deployment_id>")
def deployment(deployment_id):
    """ The purpose of this route is to print all details linked to an ec2
        instance used for deployment
    """

    client = boto3.client('codedeploy')
    response = client.list_deployment_instances(deploymentId=deployment_id)

    instances_list = response['instancesList']

    if instances_list:
        response = client.batch_get_deployment_instances(deploymentId=deployment_id,
                                                         instanceIds=instances_list)

    return render_template('deployments_instance.html',
                           items=response['instancesSummary'])


if __name__ == "__main__":
    app.run(host='127.0.0.1', port=80)
