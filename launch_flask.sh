#! /usr/bin/env bash

ENV=foo

# Normalement si tu as python d'installé le binaire pip devrait etre dans ton PATH
pip install virtualenv --user

# la je te fais installer un virtualenv pour creer un namespace sur les libs et pas polluer ton OS
virtualenv ${ENV} -ppython3

# On l'active
source ${ENV}/bin/activate

# On install les libs
pip install boto3==1.5.31 Flask==0.12.2

# Je pars du principe que les fichiers ~/.aws/config et ~/.aws/credentials sont presents
# sinon il faut les setter en variable d'env
FLASK_APP=app/main.py FLASK_DEBUG=1 flask run --host=0.0.0.0 --port=8081
